##  𓇢𓆸 HOMEWORK 11 𓇢𓆸

 ### Nombre Completo: _Maria Betzabe Ortega Soto_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  
 ### CI: _10476574_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟   
 ### RU: _114234_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  

###  📜 COMPONENTS: 📜
- [X] Header.tsx
- [X] Button.tsx
- [ ] Footer.tsx
- [X] BestSellingPlants.tsx
- [X] AboutUs.tsx
- [ ] Categories.tsx
- [ ] Reseñas.tsx