## **PROYECTO DE REACT AUX SIS313 𓍯𓂃** 
**Created by:** Univ. Maria Betzabe Ortega Soto
### **LINK DEL TEMPLATE EN FIGMA 𓍯𓂃**
https://www.figma.com/design/Ndql287R8tmPxKzEh57ovM/E-Commerce-Plant-Shop-Website-(Community)?node-id=0-1&t=aRN9u2GPOBzG7T2r-1
### **LINK DEL REPOSITORIO PRINCIPAL 𓍯𓂃**
https://gitlab.com/mariabetzabeortegasoto/aux-sis313g1-i24-mariabetzabeortegasoto.git
### **AVANZES DEL TEMPLATE 𓍯𓂃**
- [X] Header
- [X] Best Selling Plants Section
- [X] About us Section
- [ ] Categories Section
- [ ] Reseñas Section
- [ ] Footer
### **Link de netlify 𓍯𓂃**
https://proyectreactwithmaria.netlify.app/