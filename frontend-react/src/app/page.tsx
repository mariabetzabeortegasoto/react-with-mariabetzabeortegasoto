import Image from "next/image";
import styles from "./page.module.css";
import BestSellingPlants from "@/components/BestSellingPlants";
import Buttonseemore from "@/components/Buttonseemore";
import AboutUs from "@/components/AboutUs";

export default function Home() {
  return (
    <main className={styles.main}>
      <section className="header">
          <div className="header-1">
              <h1>Buy your dream plants</h1>
              <div>
                <div>
                  <h3>50+</h3>
                  <h4>Plant Species</h4>
                  </div>
                  <div id="Image-line">
                    <Image src="/images/line-header.svg" alt=""  width={100}height={100}/>
                  </div>
                  <div>
                  <h3>100+</h3>
                  <h4>Customers</h4>
                </div>
              </div>
              <div id="Search">
                 <Image src="/images/buscador.svg" alt=""  width={500}height={500}/>
              </div>
          </div>
          <div className="header-2">
            <div>
              <Image src="/images/rectangle-header.svg" alt="" width={410}height={410}/>
            </div>
            <div>
                <Image src="/images/plant-header.svg" alt="" width={550}height={550}/>
            </div>
            <div>
                <Image src="/images/flecha1.svg" alt="" width={200}height={200} />
            </div>
            <div>
                <Image src="/images/flecha2.svg" alt="" width={190}height={190} />
            </div>
          </div>
      </section>
     <section className="Best-Selling-Plants">
          <div>
            <h3>Best Selling <br />
             Plants</h3>
            <p>Easiest way to <br />healthy life by buying <br /> your favorite plants</p>
            <Buttonseemore/>
          </div>
          <div className="List">
            <BestSellingPlants imageURL="/images/plant1.svg" title="Natural Plants" precio="₱ 1,400.00"/>
            <BestSellingPlants imageURL="/images/plant2.svg" title="Artificial Plants" precio="₱ 900.00"/>
            <BestSellingPlants imageURL="/images/plant3.svg" title="Artificial Plants" precio="₱ 3,500.00"/>
          </div>
     </section>
     <section className="About-Us">
          <div>
              <h3>About Us</h3>
              <p>Order now and appreciate the beauty of nature</p>
          </div>
          <div className="List2">
              <AboutUs icon="/images/planticon.svg" title="Large Assortment" paragraph="we offer many different types of products with fewer variations in each category."/>
              <AboutUs icon="/images/boxicon.svg" title="Fast & Free Shipping" paragraph="4-day or less delivery time, free shipping and an expedited delivery option."/>
              <AboutUs icon="/images/phone.svg" title="24/7 Support" paragraph="answers to any business related inquiry 24/7 and in real-time."/>
          </div>
     </section>
    </main>
  );
}
