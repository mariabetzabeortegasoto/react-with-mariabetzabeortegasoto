import FeatherIcon from "feather-icons-react"

interface itemFooterProps {
    items: string[];
    title:string;
}
const ItemFooter = (props:itemFooterProps) => {
    return (
        <div>
             <h6>{props.title}</h6>
            <ul>
                {props.items.map((item:string,index:number)=>{
                    return <li key={index}>
                        <a href="#">{item}</a>
                    </li>
                })}
            </ul>
        </div>
    )
}
export const Footer =()=>{
    return(
        <footer>
            <div className="footer">
                <div>
                    <h2>GREENMIND</h2>
                    <p>We help you find <br /> your dream plant</p>
                    <div className="iconsfooter">
                        <span><FeatherIcon size={25} icon="facebook"  fill="#1e1e1e" strokeWidth={0.2} /></span>
                        <span><FeatherIcon size={25} icon="instagram" /></span>
                        <span><FeatherIcon size={25} icon="twitter" fill="#1e1e1e" strokeWidth={0.2}/></span>
                    </div>
                    <div id="paragraphfooter">
                        <p>2023 all Right Reserved Term of use GREENMIND (. ❛ ᴗ ❛.)</p>
                    </div>
                </div>
                <div>
                    <ItemFooter title="Information" items={["About","Product","Blog"]}/>
                    <ItemFooter title="Company" items={["Community","Career","Our story"]}/>
                    <ItemFooter title="Contact" items={["Getting Started","Pricing","Resources"]}/>
                </div>
            </div>
        </footer>
    )
}

