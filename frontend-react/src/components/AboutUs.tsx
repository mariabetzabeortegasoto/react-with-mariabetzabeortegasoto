import Image from "next/image";
export default function AboutUs(props:{icon:string,title: string, paragraph: string }){
    return(
        <div className="list-aboutus">
             <Image src={props.icon} alt="" width={100} height={100}/>
             <h4>{props.title}</h4>
             <p>{props.paragraph}</p>
        </div>
    )
}