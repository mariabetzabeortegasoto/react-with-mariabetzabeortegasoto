import Image from "next/image";
export default function BestSellingPlants(props:{ imageURL: string, title:string, precio: string}){
    return(
        <div className="List-Plants">
            <Image src={props.imageURL} alt="" width={350} height={450}/>
            <h5>{props.title}</h5>
            <p>{props.precio}</p>
        </div>
    )
}