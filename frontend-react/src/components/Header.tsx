import FeatherIcon from "feather-icons-react";
import Image from "next/image";
import Link from 'next/link';
const styleNav={
}

export function Header(){
    return(
    <div className="nav-header" style={styleNav}>
        <div>
            <h3>
                GREENMIND
            </h3>
            <ul>
                <li>
                    <Link href="/" className='active'>Home</Link>
                </li>
                <li>
                    <Link href="/" className='active'>Products</Link>
                </li>
                <li>
                    <Link href="/" className='active'>Contacts</Link>
                </li>
            </ul>
        </div>
        <div>
            <i><FeatherIcon icon="shopping-cart" /></i>
            <i><FeatherIcon icon="user" /></i>
            <Image src="/images/line-header.svg" alt="" width={20} height={20} />
            <Image src="/images/vector-header.svg" alt="" width={20} height={20} />
        </div>
    </div>
    )
}